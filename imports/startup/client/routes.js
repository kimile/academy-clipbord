"use strict";

import {Router} from "meteor/iron:router";

import '../../ui/pages/rooms/room';

Router.configure({
    layoutTemplate: 'main'
});

Router.route('/room/:id', {
    name: 'Room.show',
    template: 'Room',
    data: function () {
        return {
            roomId: this.params.id
        }
    }
});

Router.route('/', {
    name: 'Clipboard.default',
    template: 'ClipboardRoot'
});