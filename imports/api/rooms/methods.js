import {Clip} from "./collections";

Meteor.methods({
    addClip: function (roomId, user, code, type) {
        if (!roomId, !user || !code || !type) throw new Meteor.Error('missing-fields', 'user, code and type are required');

        let clip = new Clip(null, roomId, user, code, type);
        clip.save();
        return clip.id;
    }
});