import {Clip} from "./collections";

Meteor.publish('Room.Clips', function (roomId) {
    return Clip.listForRoomCursor(roomId)
});