import {Meteor} from "meteor/meteor";

const RoomsCollection = new Meteor.Collection('rooms', {idGeneration: 'STRING'});
class Room {

    constructor(id, name, url) {
        this._id = id;
        this._name = name;
        this._url = url;
    }

    /**
     *
     * @param id
     * @return {Room}
     */
    static find(id) {
        return Room.fromJSON(RoomsCollection.findOne({_id: id}));
    }

    /**
     *
     * @return {{name: *, _id: *, url: *}}
     */
    toJSON() {
        return {
            _id: this.id, name: this.name, url: this.url
        }
    }

    /**
     *
     * @param json
     * @return {null|Room}
     */
    static fromJSON(json) {
        if (!json) return null;
        return new Room(json._id, json.name, json.url)
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }
}

const ClipsCollection = new Meteor.Collection('clips', {idGeneration: 'STRING'});
class Clip {

    constructor(id, roomId, owner, content, type, createdAt) {
        this._id = id;
        this._roomId = roomId;
        this._owner = owner;
        this._content = content;
        this._type = type;
        this._createdAt = createdAt;
    }

    /**
     *
     * @param id
     * @return {Clip}
     */
    static find(id) {
        return Clip.fromJSON(ClipsCollection.findOne({_id: id}));
    }


    /**
     *
     * @param roomId
     * @return {Array<Clip>}
     */
    static listForRoom(roomId) {
        let clips = Clip.listForRoomCursor(roomId).fetch(), result = [];
        for (let clip of clips) {
            result.push(Clip.fromJSON(clip));
        }
        return result;
    }

    /**
     *
     * @param roomId
     * @return {Mongo.Cursor}
     */
    static listForRoomCursor(roomId) {
        return ClipsCollection.find({roomId: roomId}, {sort:{createdAt: -1}})
    }

    /**
     *
     * @return {{name: *, _id: *, url: *}}
     */
    toJSON() {
        return {
            _id: this.id, roomId: this.roomId, owner: this.owner, content: this.content, type: this.type
            , createdAt: this.createdAt
        }
    }

    /**
     *
     * @param json
     * @return {null|Clip}
     */
    static fromJSON(json) {
        if (!json) return null;
        return new Clip(json._id, json.roomId, json.owner, json.content, json.type, json.createdAt)
    }

    /**
     *
     * @return {*}
     */
    save() {
        if (!this.id) {
            let json = this.toJSON();
            delete json._id;
            this.id = ClipsCollection.insert(json);
        } else {
            ClipsCollection.update({_id: this.id}, {$set: this.toJSON()})
        }
        return this.id;
    }


    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get roomId() {
        return this._roomId;
    }

    set roomId(value) {
        this._roomId = value;
    }

    get owner() {
        return this._owner;
    }

    set owner(value) {
        this._owner = value;
    }

    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }

    get type() {
        return this._type;
    }

    set type(value) {
        this._type = value;
    }

    get createdAt() {
        return this._createdAt;
    }

    set createdAt(value) {
        this._createdAt = value;
    }
}


export {
    Room, Clip
}