import './room.html'
import './room.css';
import {Clip} from "../../../api/rooms/collections";

function getUserName() {
    return localStorage.getItem('user');
}

function storeUser(name) {
    localStorage.setItem('user', name);
}

Template.Room.onCreated(function () {
    console.log('here', this.data);
    Meteor.subscribe('Room.Clips', this.data.roomId);
});

Template.Room.onRendered(function () {
    $('select').material_select();
});

Template.Room.helpers({
    clips: function () {
        console.log(Clip.listForRoom(this.roomId), this.roomId);
        return Clip.listForRoom(this.roomId);
    }
});

Template.Room.events({
    'click button': function (e) {
        let $code = $('textarea');
        let $type = $('select');
        let $user = $('#username');
        let code = $code.val();
        let type = $type.val();
        let user = getUserName();
        if (!user) {
            user = $user.val();
            storeUser(user);
        }

        $code.removeClass('invalid');
        $type.removeClass('invalid');
        $user.removeClass('invalid');

        if (!user) $user.addClass('invalid');
        if (!code) $code.addClass('invalid');
        if (!type) $type.addClass('invalid');


        Meteor.call("addClip", this.roomId, user, code, type, function (err, data) {
            console.log(err, data);
            // intentially left blank
        });
    },

    'change .room-submit-clip': function () {
    }
});

Template.CreateClip.helpers({
    hasNoName: function () {
        return !getUserName()
    }
});