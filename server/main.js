import { Meteor } from 'meteor/meteor';

import '../imports/api/rooms/methods';
import '../imports/api/rooms/publications';
import '../imports/api/rooms/collections';

Meteor.startup(() => {
  // code to run on server at startup
});
